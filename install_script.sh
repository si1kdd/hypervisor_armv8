#!/usr/bin/env bash

echo "[!] This script is for CentOS, please exit if you are using Ubuntu....."
sleep 5
echo "[*] Install xterm, cross compiler for ARMv8, and telnet"
sudo yum install xterm telnet gcc-aarch64-linux-gnu
