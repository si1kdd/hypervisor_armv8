#!/usr/bin/env bash
make clean 
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- \
    BOOTARGS='"root=/dev/vda consolelog=9 rw console=ttyAMA0"' \
    FDT_SRC=foundation-v8.dts IMAGE=image-foundation_VIRTIO.axf
