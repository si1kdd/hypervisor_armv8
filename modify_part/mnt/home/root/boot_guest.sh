#!/usr/bin/env bash

rm -r -f ./.lkvm/*
./lkvm-static run --kernel Image -d guest.img -c 1 -m 512 --console virtio \
    --params "earlyprintk=smh console=hvc0 root=/dev/vda init=/bin/sh"
